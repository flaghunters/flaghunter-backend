'use strict';

/**
 * Var declarations
 */
var boardsToProcess = [{
        board: "int"
    },
    {
        board: "sp"
    },
    {
        board: "pol"
    },
    {
        board: "bant"
    }];

var catalogUrlPrefix = "https://a.4cdn.org/";
var catalogUrlSuffix = "/catalog.json";

var threadUrlPrefix = "https://a.4cdn.org/";
var threadUrlMidfix = "/thread/";
var threadUrlSuffix = ".json";

var flagResultFile = 'flags-results.json';
var attendanceFile = 'public/attendance.json';
var countryDataFile = 'country_data.json';

/**
 * Require, imports, etc.
 */
const requestify = require('requestify');
const fs = require('fs');

/**
 * Append time to logs
 */
require('console-stamp')(console, 'yyyy/mm/dd HH:MM:ss.l');

//====================================
//Actual functionality starts here
//====================================

//example buildup
var results = [{
    country_code: "XX",
    post_nr: "2",
    thread_nr: "1",
    board: "int"
}, {
    country_code: "AQ",
    post_nr: "4",
    thread_nr: "3",
    board: "int"
}, {
    country_code: "AQ",
    post_nr: "6",
    thread_nr: "5",
    board: "pol"
}, {
    country_code: "AQ",
    post_nr: "7",
    thread_nr: "7",
    board: "bant"
}, {
     country_code: "",
     post_nr: "8",
     thread_nr: "8",
     board: "bant"
}];

// run
update();
// testing purposes
//submit();
//updateAttendance();

/**
 * Initial method to start the update.
 */
function update() {
    results = null;
    for (var i = 0; i < boardsToProcess.length; i++) {
        console.log(i);
        processBoard(boardsToProcess[i].board);
    }
}

/**
 * Get catalog for board, falls through to processing catalog.
 * @param board The board to check.
 */
function processBoard(board) {
    requestify.get(catalogUrlPrefix + board + catalogUrlSuffix)
        .then(function (response) {
            catalogHttpsReceived(JSON.parse(response.body), board);
        });
}

/**
 * Process the catalog data as JSON.
 * @param jsonData the catalog JSON data.
 * @param board the board to parse
 */
function catalogHttpsReceived(jsonData, board) {
    for (var i = 0; i < jsonData.length; i++) {
        if (typeof jsonData[i] !== 'undefined') {
            var threads = jsonData[i].threads;
            for (var j = 0; j < threads.length; j++) {
                //we got our thread object, process every thread now
                console.log(j);
                setTimeOutForCheckThread(threads[j], board, i + 1, j + 1, threads.length);
            }
        }
    }
}

/**
 * Set timeout for checking a thread.
 * @param thread
 * @param board
 * @param i used for delay
 * @param j used for delay
 * @param totalJ used for delay
 */
function setTimeOutForCheckThread(thread, board, i, j, totalJ) {
    setTimeout(function () {
        checkThread(thread, board);
    }, 1050 * (j + (i - 1) * totalJ));
    console.log(1050 * (j + (i-1)*totalJ));
}

/**
 * Actual check thread method.
 * @param thread
 * @param board
 */
function checkThread(thread, board) {
    console.log(thread.no);
    requestify.get(threadUrlPrefix + board + threadUrlMidfix + thread.no + threadUrlSuffix)
        .then(function (response) {
            processThread(JSON.parse(response.body), board);
        });
}

/**
 * Process the thread for replies
 * @param threadAsJson
 * @param board
 */
function processThread(threadAsJson, board) {
    console.log("process " + threadAsJson.posts[0].no);
    for (var i = 0; i < threadAsJson.posts.length; i++) {
        if (results === null || results === 'undefined') {
            if (threadAsJson.posts[i].country != null && threadAsJson.posts[i].country != 'undefined') {
                results = [];
                var result = {
                    country_code: threadAsJson.posts[i].country,
                    thread_nr: threadAsJson.posts[0].no,
                    post_nr: threadAsJson.posts[i].no,
                    board: board
                };

                results.push(result);
            }
        } else {
            var resultIsIn = false;
            for (var j = 0; j < results.length; j++) {
                if (results[j].country_code === threadAsJson.posts[i].country && results[j].board === board) {
                    resultIsIn = true;
                }
            }

            //console.log("result for " + threadAsJson.posts[0].no + " / " + threadAsJson.posts[i].no + " is " + resultIsIn);

            if (!resultIsIn) {
                var result2 = {
                    country_code: threadAsJson.posts[i].country,
                    thread_nr: threadAsJson.posts[0].no,
                    post_nr: threadAsJson.posts[i].no,
                    board: board
                };

                console.log("add " + JSON.stringify(result2));

                results.push(result2);
            }
        }
    }

    //console.log(results);
    setOrResetEndCounter();
}

//counter var
var timeOutCounter;
var timeoutForUpdate = 10000;

/**
 * Init or reset the counter, will fire 10s after the last call
 */
function setOrResetEndCounter() {
    if (timeOutCounter !== null) {
        clearTimeout(timeOutCounter);
    }
    timeOutCounter = setTimeout(submit, timeoutForUpdate);
}

/**
 * Submit the results for a run.
 */
function submit() {
    if (results !== null && results !== 'undefined') {
        console.log("update processed with count " + results.length);

        var resultsOutput = JSON.parse(fs.readFileSync(countryDataFile));

        // format to single country per entry
        for (var i = 0; i < results.length; i++) {

            if (results[i].country_code === '' || results[i].country_code === undefined ) {
                continue;
            }

            var toEdit = {};

            for (var j = 0; j < resultsOutput.length; j++) {
                if (resultsOutput[j].country_code === results[i].country_code) {
                    toEdit = resultsOutput[j];
                    resultsOutput.splice(j, 1);
                }
            }

            toEdit["post_link_" + results[i].board] = "https://boards.4chan.org/" + results[i].board + "/thread/" +
                results[i].thread_nr + "#p" + results[i].post_nr;
            toEdit["post_nr_" + results[i].board] = results[i].post_nr;
            toEdit.country_code = results[i].country_code;

            toEdit.upAttendance = true;

            resultsOutput.push(toEdit);
        }

        var countriesData = JSON.parse(fs.readFileSync(countryDataFile));

        for (var i = 0; i < resultsOutput.length; i++) {
            if (resultsOutput[i].upAttendance) {
                for (var j = 0; j < countriesData.length; j++) {
                    if (resultsOutput[i].country_code === countriesData[j].country_code) {
                        if (countriesData[j].attendance) {
                            countriesData[j].attendance += 1;
                        } else {
                            countriesData[j].attendance = 1;
                        }
                        resultsOutput[i].attendance = countriesData[j].attendance;
                    }
                }
            }
        }

        // write to file
        fs.writeFile(flagResultFile, JSON.stringify(resultsOutput), (err) => {
            if (err) throw err;
        });
        fs.writeFile(countryDataFile, JSON.stringify(countriesData), (err) => {
            if (err) throw err;
        });

        updateAttendance();

    } else {
        console.log("Error. 4chan down?");
    }
}

// increase Attendence
function updateAttendance() {
    var attendance = JSON.parse(fs.readFileSync(attendanceFile));
    attendance.runs += 1;
    fs.writeFile(attendanceFile, JSON.stringify(attendance), (err) => {
        if (err) throw err;
    });
}
